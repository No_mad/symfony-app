<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Text;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Subcomment::class, mappedBy="comment")
     */
    private $subcomments;

    public function __construct()
    {
        $this->subcomments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->Text;
    }

    public function setText(string $Text): self
    {
        $this->Text = $Text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Subcomment[]
     */
    public function getSubcomments(): Collection
    {
        return $this->subcomments;
    }

    public function addSubcomment(Subcomment $subcomment): self
    {
        if (!$this->subcomments->contains($subcomment)) {
            $this->subcomments[] = $subcomment;
            $subcomment->setComment($this);
        }

        return $this;
    }

    public function removeSubcomment(Subcomment $subcomment): self
    {
        if ($this->subcomments->removeElement($subcomment)) {
            // set the owning side to null (unless already changed)
            if ($subcomment->getComment() === $this) {
                $subcomment->setComment(null);
            }
        }

        return $this;
    }
}
