<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Subcomment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubcommentController extends AbstractController
{
    /**
     * @Route("/{id}/createSubcomment", name="create_subcomment", requirements={"id"="\d+"})
     */
    public function createSubcomment(int $id, Request $request, ValidatorInterface $validator)
    {
        if ($request->getMethod() == 'POST') {
            $subcomment = new Subcomment();

            $subcomment->setText($request->request->get('Text'));
            $subcomment->setCreatedAt(date_create_immutable());

            /** @var \App\Entity\User $user */
            $user = $this->getUser();
            $subcomment->setOwner($user);

            $commentRepository = $this->getDoctrine()->getRepository(Comment::class);
            $commentParent = $commentRepository->find($id);
            $subcomment->setComment($commentParent);

            $errors = $validator->validate($subcomment);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;

                return $this->render('subcomment/createSubcomment.html.twig', ['error' => $errorsString, 'id' => $id]);
            } else {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($subcomment);
                $entityManager->flush();

                return $this->redirectToRoute('home');
            }
        }
        return $this->render('subcomment/createSubcomment.html.twig', ['error' => '', 'id' => $id]);
    }
}
