<?php

namespace App\Controller;

use App\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommentController extends AbstractController
{
    /**
     * @Route("/phpinfo", name="phpinfo")
     */
    public function index()
    {
        phpinfo();
    }

    /**
     * @Route("/", name="home")
     */
    public function getAllComments()
    {
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAll();
        return $this->render('comment/home.html.twig', ['comments' => $comments, 'msg' => '']);
    }

    /**
     * @Route("/{id}", name="show_my_comments", requirements={"id"="\d+"})
     */
    public function getUserComments(int $id)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        if($user->getId() == $id) {
            $allComments = $this->getDoctrine()->getRepository(Comment::class)->FindAll();
            $comments = array();
            foreach ($allComments as $comment ){
                if($comment->getOwner()->getUsername() == $user->getUsername()) {
                    array_push($comments, $comment);
                }
            }
            return $this->render('comment/home.html.twig', ['comments' => $comments, 'msg' => '']);
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/createComment", name="create_comment")
     */
    public function createComment(Request $request, ValidatorInterface $validator): Response
    {
        if ($request->getMethod() == "POST") {
            $comment = new Comment();

            $comment->setText($request->request->get('Text'));
            /** @var \App\Entity\User $user */
            $user = $this->getUser();
            $comment->setOwner($user);
            $comment->setCreatedAt(date_create_immutable());

            $errors = $validator->validate($comment);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;

                return $this->render('comment/createComment.html.twig', ['error' => $errorsString]);
            } else {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($comment);
                $entityManager->flush();

                return $this->redirectToRoute('home');
            }
        }
        return $this->render('comment/createComment.html.twig', ['error' => '']);
    }

    /**
     * @Route("/editComment/{id}", name="edit_comment", requirements={"id"="\d+"})
     */
    public function editComment(Request $request, ValidatorInterface $validator, int $id): Response
    {
        if ($request->getMethod() == 'POST') {
            $commentRepository = $this->getDoctrine()->getRepository(Comment::class);
            $comment = $commentRepository->find($id);

            $comment->setText($request->request->get('Text'));
            $comment->setCreatedAt(date_create_immutable());

            $errors = $validator->validate($comment);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;

                return $this->render('comment/editComment.html.twig', ['error' => $errorsString, 'id' => $id]);
            } else {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->flush();

                return $this->redirectToRoute('home');
            }
        }
        return $this->render('comment/editComment.html.twig', ['error' => '', 'id' => $id]);
    }
}
